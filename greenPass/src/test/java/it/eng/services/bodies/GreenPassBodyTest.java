package it.eng.services.bodies;

import java.sql.SQLException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import it.eng.services.bodies.dao.GreenPassDao;
import it.eng.services.bodies.dao.dto.InitFilterResponse;

@RunWith(MockitoJUnitRunner.class)
public class GreenPassBodyTest {
	
	@Test
	public void initFiler() {
		InitFilterResponse response = new InitFilterResponse();
		GreenPassDao dao = new GreenPassDao(any());
		
		when(dao.initFilter().thenThrow(SQLException.class));
	}
}
