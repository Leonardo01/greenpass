package it.eng.services.bodies.dao.dto;

import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.any;

import org.junit.Test;

@RunWith(MockitoJUnitRunner.class)
public class InitFilterResponseTest {
	
	@Test
	public void InitFilterResponseTest() {
		InitFilterResponse ifr = new InitFilterResponse();
		ifr.setSocieta(any());
		ifr.getSocieta();
		ifr.setFiliale(any());
		ifr.getFiliale();
		ifr.setIndirizzoFiliale(any());
		ifr.getIndirizzoFiliale();
		ifr.setDistaccamento(any());
		ifr.getDistaccamento();
		ifr.setIndirizzoDistaccamento(any());
		ifr.getIndirizzoDistaccamento();
	}
}
