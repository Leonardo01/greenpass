package it.eng.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

import it.eng.services.bodies.dao.dto.Utente;
import it.eng.utility.Jsso;
import it.eng.utility.LoginDAO;



/**
 * Servlet implementation class Login
 */
@WebServlet("/USER")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger log = Logger.getLogger(Login.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Login() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @throws IOException
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		LoginDAO dao  = new LoginDAO();
		Jsso j = dao.login(request);
		PrintWriter out = response.getWriter();
		
		if(Jsso.utente == null) {
			out.write("false");
		}
		
//		int abilitazioneUtente = j.getAbilitazioneUtente();
		
//		if (Jsso.ruolo == null || (request.getParameter("abilitazione") != null &&  Integer.parseInt(request.getParameter("abilitazione").toString()) != Jsso.ruolo)) {
//            Jsso.ruolo = Integer.parseInt(request.getParameter("abilitazione").toString());
//        }
		Jsso.ruolo =  Jsso.getAbilitazioneUtente();
		Utente utente = new Utente(Jsso.nome, Jsso.cognome, Jsso.utente, Jsso.ruolo);
//		log.info("Sono in abilitato "+ abilitazioneUtente);

		out.write(new Gson().toJson(utente));
		out.flush();
		out.close();
//		log.info(j.utente + " " + "isabilitato=" + abilitazioneUtente +"Privilegi" + j.entirePrivilege.toString());	
	}
}
