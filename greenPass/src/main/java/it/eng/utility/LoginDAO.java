package it.eng.utility;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;


public class LoginDAO
{
	private static Logger log = Logger.getLogger(LoginDAO.class);
	
	public Jsso login(HttpServletRequest request)
	{
	
		HttpSession session = request.getSession();
//		Jsso j = (Jsso) session.getAttribute("jsso");
//		if(j!=null)
//		{
//			j.isLogged = true;
//			return j;
//		}
		
		Jsso j = new Jsso();
		j.getUser(request, "GPE30");
		if(j.utente == null || j.utente.length()==0)
		{
			return null;
		}

		session.removeAttribute("jsso");
		session.setAttribute("jsso", j);
		session.setAttribute("operatorePP", j.onlyPrivilege.toString());
		session.setAttribute("BW", j.profileBW);
		
		
		log.info("Entrato-------->" + j.utente + " BW=" + j.profileBW);
		
		return j;
	}
}
