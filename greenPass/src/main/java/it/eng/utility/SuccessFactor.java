package it.eng.utility;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

//import it.eng.services.bodies.dao.dto.SelezioneModello;

public class SuccessFactor {
	 
	private static Logger log = Logger.getLogger(SuccessFactor.class);
	
	private static final String SYSTEM_PROPERTIES = "DD_GPE30_ambiente_ISP";
	
	private static final String PROD =   "http://datapower-ext-prod.gtm.corp.sanpaoloimi.com:5670/";
	private static final String SYSTEM = "http://datapower-ext-test.syssede.systest.sanpaoloimi.com:5670/";
	private static final String SVIL =   "http://datapower-ext-svil.syssede.systest.sanpaoloimi.com:5670/";
	
	private static final String API_KEY = "ODAwYTBiZDU1YWI3NGU1ZDAxNzI1MjJlNTE0Mw";
	private static final String USER_ID = "arecruiting";
	private static final String PRIVATE_KEY = "TUlJRXZnSUJBREFOQmdrcWhraUc5dzBCQVFFRkFBU0NCS2d3Z2dTa0FnRUFBb0lCQVFDdlp0aXc1OGtNMzQ5Nmlucy9LeVhKZXJyaytmckNuR0ZFbEZGTHk2WDFPM1hFNEpaQ3EzSnE0VGdtNzZBVWFkK2RxZFpqVHhhbjBiemw0K09LVk5TVGNzNmo5NFJVSVc5UkkrL3htWGtuZ0kyNXc3cFhmVU9laEYvZzZzZ0RNY2N2c0Y0UTZtWWI0VnNKcnJIUUVCN05ETGlxdVJJNlk5ZHE4b0kyd2U1cTdPVmcybXMxUURSbDRmbkRIdDB3elFqNWFXQlo4Y1JSaGxpTWk4UWl4dXlhNmhlOEwzdkZLUGNHOVNldVVnWklicG5zbEpNTXhsblhqK0NKdGdkT20raHR0NEhLaVZWdmdTWWZVemJvK1NRejNxVVVvZ2FWYWZwYnhMSTJ6b1BRVXBscFBWYVJxZVhZVzhzWU5GQVByN0JUSnNYNTZRbWtodWpnMzJMV1dPc2RBZ01CQUFFQ2dnRUJBSmRuRlVDT3NwSzBIdXROd2RqaTY3a2dnVmJKR05VSG0wWlJLR05lcENBc2RSbStLclhkdStQYUNGaERtMUJEbTB4Q245WVZISjg3Z2V5R2E1Q3FiTnZoeTlEcEJJYUNYYzRTVVRtZ21sMm9yNjFiNytzNUt6Y1hXSk9TMVpjOFRvMHhsZURRRTllWkRMWXMzenVUenFKOGlxS0RJZUxZNzhkWEVXTW12eC8zYnczYkhsOEhPT3VYUjRPM0xWUkk0emJ1WEZveXRPQkx2L2VEaGFUTEltRHFXV04rNnpNbytWRUJYZUZJRkliSWhFbC9tSFB2THRIdlFKK0FXSTF0ZFUyZVUveU5WaWVOa0M1dVhUbnI4NUlYaHc1TFNGb2FySUVhOXBFc040SC9yaTBjaGNCTDBsdmREWjNhZ2ZPa2x1akZQelhsNWJDZnJtRGV1WnltYjRFQ2dZRUE4K0tmd0pxMXlCc0VnWnl4TnM3bTFZbyt4dy90R2lnVUZ0NHhUM0pjR3hzK2FzWnlOdUMxYUVZTG9NVnoxRmUySGkxWGdZRGorZU5BQTJVUHBRZDBXTWtiRVN5UjZtdkJDTkpjZVYyNnlpb1BwanlyRTFNSGFLOGFLSlVkNkRQWmp3akhZSlhQbVA3Mk9ZUUlvS08yRUVHQ0VKRUNZYWRIYUp4UmRvTUt4ODBDZ1lFQXVCMVpiY2cyYWhKQ3k5MXB0VWhJMzlDNTV2eTJDNCsxTFM4Nm95TFZSdDVQa3ZDVU90MGpPdTlQN3BuWW51WHZ2Z2pXOHdlTHpSZVN3NnZGNDZad3k5eVp3TzNsaFlMY045c0RPMkpQa1ltSERoaEx0Um9GN3BsdmpqT2Nmbm5XM21MN2wwODJRSys2MW9YdTYyUXBjQk5tQmZsbVptNnpxL1pWUEhqMHdKRUNnWUF1azRVMjMxVUFGa2pTQi83QUhOdHd4S29na2lZUjdMMkpjSDJ5VW9ETW5ITmJLUEwvRUFNVkd1dDFsTC8rT1pJS1NWYUE0UUNvNUlHQkpIZk1hSlFMc0x4K2x5aDJ3SVZwRGVPak9nZEdQS29vY1VXcXR4eWZIWEc5ZGx6eno1TXBBZUF4emVxTHFZV1VqbWFuWnh5S0ZqaWt1VXpmSVhjOXZ6eS9pVjh4c1FLQmdRQ1YwNngyM2pzSnpwcDA5RldWcEtGSkliNW03cmZtYUtmemdUK2lpakl0WGJRN3kzT1ZBMmdzQ1p0dWU5d0FVdlhxRzU5d1htK3lFc0RvWXR6RENTczVldWZVVE1RL0JlTFh3VzY3NEtFSHVuY2hUVHd1d0dLYmJGbVB5d2hMRkRONi9lbm5sQnArUVlNMkYvLzVwcVRpQTZjK3JWcGJlVExWZWJ5YlhZTXZRUUtCZ0FXajNuaGlMQ3dZQU9YakgyK21rbmdsZmNrRmlsMUd6VEoxVDU5UkVSemtrb1RQLzlLV01DVnBaQ3hEWkMxMDg5NEtJd2VlVnk3VGZYaW40UnlhOHR0WldWd1VyWWxFS0NOczlFSlNFa25JemZDRkkwOGt4cy9raDhqNkxtcVY3b1VDZ0lFK3pDTVpTWXZVVzdPazBUcnlXK0VwUVJ0anIwaUNaZVBHVWxRdSMjI2ludGVzYXNhbnBUMQ==";
	
	private static final String INTERNA = "2025";
	private static final String ESTERNA = "2026";
	private static final String PARALLELA = "2027";
	
	public static String getAssertion() throws Exception {
        String path = "oauth/idp";
        URL url = getEnvironment(path);
		HttpURLConnection  connection = (HttpURLConnection) url.openConnection();		
		
		connection.setRequestProperty("Media-Type", "x-www-form-urlencoded");
		connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		connection.setRequestMethod("POST");
		connection.setDoOutput(true);
		String jsonInputString = "client_id="+API_KEY+"&" + 
								"user_id="+ USER_ID+"&" + 
								"token_url=https://api2preview.sapsf.eu/oauth/token&" + 
								"private_key="+PRIVATE_KEY;
		
		OutputStream os = connection.getOutputStream();
	    byte[] input = jsonInputString.getBytes("utf-8");
	    os.write(input, 0, input.length);	
	    
	    StringBuilder response = new StringBuilder();
	    try {
		    BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream(), "utf-8"));
		    String responseLine = null;
		    while ((responseLine = br.readLine()) != null) {
		        response.append(responseLine.trim());
		    }
	    } catch(Exception ex) {log.error(ex.getMessage());}
	    
	    return response.toString();
	}
	
	public static String getToken() throws Exception {
		String path = "oauth/token";
	    URL url = getEnvironment(path);
		HttpURLConnection  connection = (HttpURLConnection) url.openConnection();		
		
		connection.setRequestProperty("Media-Type", "x-www-form-urlencoded");
		connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		connection.setRequestMethod("POST");
		connection.setDoOutput(true);
		String jsonInputString = "company_id=intesasanpT1&" + 
				"client_id="+API_KEY+"&" + 
				"grant_type=urn:ietf:params:oauth:grant-type:saml2-bearer&" + 
				"user_id="+ USER_ID+"&"  + 
				"assertion="+ getAssertion();
		
		OutputStream os = connection.getOutputStream();
		byte[] input = jsonInputString.getBytes("utf-8");
		os.write(input, 0, input.length);			
		
		StringBuilder response = new StringBuilder();
		
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream(), "utf-8"));
			String responseLine = null;
			while ((responseLine = br.readLine()) != null) {
			    response.append(responseLine.trim());
			}
		} catch (Exception ex) {log.error(ex.getMessage());}
		
		JsonParser parser = new JsonParser();
		JsonElement j = parser.parse(response.toString());
		
		String token = "";
		if (j.isJsonObject()) {
			JsonObject jo = j.getAsJsonObject();
			if(jo.get("access_token")!=null) {
				token = (jo.get("access_token").getAsString());
			}	
		} 
		
		return token;
	}
	
//	public static SelezioneModello getApplication(int idApplication) throws Exception {
//		
//		String token = getToken();
//		
//		SelezioneModello sm = new SelezioneModello();
//        String path = "odata/v2/JobApplication("+idApplication+")/?$format=json";
//        URL url = getEnvironment(path);
//
//		JsonElement element = getConnection(url, token);
//		if (element.isJsonObject()) {
//			JsonObject j = element.getAsJsonObject();
//			if(j.get("d")!=null) {
//				JsonObject d = (j.get("d").getAsJsonObject());
//				sm.setIdJobApplication(d.get("applicationId").getAsInt());
//				sm.setIdCandidato(d.get("candidateId").getAsInt());
//				sm.setNome(d.get("firstName").isJsonNull() ? "" : d.get("firstName").getAsString());
//				sm.setCognome(d.get("lastName").isJsonNull() ? "" : d.get("lastName").getAsString());
//				if(!d.get("dateOfBirth").isJsonNull()) {
//					String rawDate = d.get("dateOfBirth").getAsString();
//					String dd = rawDate.replace("/Date(", "").replace(")/","");
//					SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-YYYY");
//					Date date = new Date(Long.valueOf(dd));
//					sm.setDataNascita(sdf.format(date));
//				}
//				sm.setEmail(d.get("contactEmail").isJsonNull() ? "" : d.get("contactEmail").getAsString());
//				sm.setCid(d.get("usersSysId").isJsonNull() ? 0 : d.get("usersSysId").getAsInt());
//				sm.setIdJobRequisition(d.get("jobReqId").getAsInt());
//				sm.setDescJobRequisition(getJobRequisitionDescription(d.get("jobReqId").getAsInt(), token)); 	
//			}	
//		}
//		return sm;
//	}
	
	public static String getJobRequisitionDescription(int idJobRequisition, String token) throws Exception {
		String jobTitle = null;
		String externalJob = null;
		String idTipo = null;
		String s = null;
        String path = "odata/v2/JobRequisition("+ idJobRequisition +")/jobReqLocale?$format=json";
        String pathT = "odata/v2/JobRequisition("+ idJobRequisition +")/IJMtype?$format=json";  
        URL url = getEnvironment(path);
        
		JsonElement element = getConnection(url, token);
		if (element.isJsonObject()) {
			JsonObject j = element.getAsJsonObject();
			if(j.get("d")!=null) {
				JsonObject d = (j.get("d").getAsJsonObject());
				JsonArray results = d.get("results").getAsJsonArray();
				JsonObject x = results.get(0).getAsJsonObject();
				
				jobTitle = x.get("jobTitle").getAsString();
				externalJob = x.get("externalTitle").getAsString();
			}	
		} 
		
		URL urlT = getEnvironment(pathT);
		JsonElement elementT = getConnection(urlT, token);
		if (elementT.isJsonObject()) {
			JsonObject j = elementT.getAsJsonObject();
			if(j.get("d")!=null) {
				JsonObject d = (j.get("d").getAsJsonObject());
				JsonArray results = d.get("results").getAsJsonArray();
				JsonObject x = results.get(0).getAsJsonObject();	
				idTipo = x.get("id").getAsString();
			}	
		} 
		
		if(idTipo != null && idTipo.equals(INTERNA)) 
			s = jobTitle;
	    else if (idTipo != null && (idTipo.equals(ESTERNA) || idTipo.equals(PARALLELA)))
			s = externalJob;
		
		return s;
	}
	
	public static List<Integer> getListaJobRequisition() throws Exception {
		
		String token = getToken();
		
		List<Integer> lista = new ArrayList<Integer>();
		String path = "odata/v2/JobRequisitionOperator?$format=json&$filter=userName%20eq%20%27";  
		if(Jsso.ruolo == 3) 
			path+="arecruiting";
		else 
			path+=Jsso.utente;
		
		path += "%27&$select=jobReqId,%20operatorRole";
		URL url = getEnvironment(path);


		JsonElement element = getConnection(url, token);
		if (element.isJsonObject()) {
			JsonObject j = element.getAsJsonObject();
			if(j.get("d")!=null) {
				JsonElement d = j.get("d");
				JsonObject rj = d.getAsJsonObject();
				JsonArray ja = rj.get("results").getAsJsonArray();

				for(int i =0; i<ja.size(); i++) {
					JsonObject x = ja.get(i).getAsJsonObject();
					lista.add(x.get("jobReqId").getAsInt());
				}
				try {
					List<Integer> l = callNext(rj, token);
					lista.addAll(l);
				} catch (Exception ex) {log.info("No __next URL to call");}	
			}
		}
		
		return lista;
	}
	
	public static List<Integer> callNext(JsonObject jsonObject, String token) throws Exception {
		List<Integer> lista = new ArrayList<Integer>();
		JsonElement et = getConnection(getEnvironment(jsonObject.get("__next").getAsString().replace("https://api2preview.sapsf.eu/", "")), token);
		if (et.isJsonObject()) {
			JsonObject ejt = et.getAsJsonObject();
			if(ejt.get("d")!=null) {
				JsonElement ft = ejt.get("d");
				JsonObject responseObject = ft.getAsJsonObject();
				JsonArray responseArray = responseObject.get("results").getAsJsonArray();

				for(int i =0; i<responseArray.size(); i++) {
					JsonObject x = responseArray.get(i).getAsJsonObject();
					lista.add(x.get("jobReqId").getAsInt());
				}
				try {
					callNext(responseObject, token);
				} catch (Exception ex) {log.info("No __next URL to call");}	
			}
		}
		return lista;
	}
	
	public static JsonObject postToSuccessFactor(String jsonInputString) throws Exception {
		
		String token = getToken();
		
		JsonObject j = new JsonObject();
		String path = "odata/v2/upsert/?$format=json";
		
		URL url = getEnvironment(path);
		HttpURLConnection  connection = (HttpURLConnection) url.openConnection();

		String bearerToken = "Bearer " + token;
		connection.setRequestProperty("Authorization", bearerToken);
		
		log.info("Posting to Success Factors URL: " + url);
		
		connection.setRequestProperty("Content-Type", "application/json");
		connection.setRequestMethod("POST");
		connection.setRequestProperty("Accept", "application/json");
		connection.setDoOutput(true);
		
		log.info("Invio a SF... " + jsonInputString);
		
		OutputStream os = connection.getOutputStream();
	    byte[] input = jsonInputString.getBytes("utf-8");
	    os.write(input, 0, input.length);			
	
	    StringBuilder response = new StringBuilder();
	    
	    try {
		    BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream(), "utf-8"));
		    String responseLine = null;
		    while ((responseLine = br.readLine()) != null) {
		        response.append(responseLine.trim());
		    }
	    } catch(Exception ex) {log.error(ex.getMessage());}

	    JsonParser parser = new JsonParser();
	    JsonElement je = parser.parse(response.toString());
	    if (je.isJsonObject()) {
			JsonObject jo = je.getAsJsonObject();
			if(jo.get("d")!=null) {
				JsonArray d = jo.get("d").getAsJsonArray();
				j = d.get(0).getAsJsonObject();
			}
	    }
	    log.info("SF risponde... "+j.toString());
		return j;
	}
	
	public static JsonElement getConnection(URL url, String token) throws Exception {
		
		HttpURLConnection  connection = (HttpURLConnection) url.openConnection();
		
		String bearerToken = "Bearer " + token;
		connection.setRequestProperty("Authorization", bearerToken);
		
		log.info("Calling Success Factors URL: "+ url);
		
		connection.setRequestProperty("Content-Type", "application/json");
		connection.setRequestMethod("GET");
		BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		String inputLine;
		StringBuilder buff = new StringBuilder();
		while ((inputLine = in.readLine()) != null) {
			buff.append(inputLine);
		}
		in.close();
		JsonParser parser = new JsonParser();

		return parser.parse(buff.toString());
	}
	
	public static URL getEnvironment(String path) throws Exception {
		URL url = new URL(SYSTEM+path); // DEFAULT FOR LOCALHOST 
		if (System.getProperty(SYSTEM_PROPERTIES)!=null) {
			if (System.getProperty(SYSTEM_PROPERTIES).contains("PRODUZIONE")) {url = new URL(PROD+path); log.info("AMBIENTE : PROD");}
			if (System.getProperty(SYSTEM_PROPERTIES).contains("SYSTEMTEST")) {url = new URL(SYSTEM+path); log.info("AMBIENTE : SYSTEM");}	
			if (System.getProperty(SYSTEM_PROPERTIES).contains("APPLICATION")) {url = new URL(SVIL+path); log.info("AMBIENTE : SVIL");}
		} 
		return url;
	}
}
