package it.eng.utility;

import java.io.BufferedReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import it.eng.services.bodies.dao.conn.GesConn;

public class Jsso {
	public static String utente;
	public static String nome;
	public static String cognome;
	public static Integer ruolo;
	public String operatore;
	public Properties props;
	public boolean isLogged = false;

	public String profileBW = "";
	public StringBuffer	onlyPrivilege = new StringBuffer("");
	public static StringBuffer entirePrivilege	= new StringBuffer("");
	public String visualizza = "";
	public String profileId = "";
	private static Logger log = Logger.getLogger(Jsso.class);

	public void getUser(HttpServletRequest request, String siteName){
		utente = "";
		entirePrivilege = new StringBuffer("");
		request.getSession().setAttribute("entirePrivilege", entirePrivilege);
		request.getSession().setAttribute("utente", utente);
	
		profileId = request.getHeader("SWAProfileId");
		String urlGetProfile = request.getHeader("ProfileServicesURL");
	
		log.info(" SWAProfileId " + profileId);
		log.info(" ProfileServicesURL " + urlGetProfile);
		log.info(" request.getServerName() " + request.getServerName());
		log.info(" request.getSession().getId() " + request.getSession().getId());
	
		String usrBD = request.getSession().getAttribute("1BD") != null ? request.getSession().getAttribute("1BD").toString() : "";
	
		String privBD="";	
		String pbwBD = "";
		if(request.getServerName().equals("localhost") || (usrBD!=null && !usrBD.equals(""))) {
			if (!usrBD.equals("")) {
				if (!usrBD.equals("")) profileId = usrBD; 
				if (!privBD.equals("")) entirePrivilege = new StringBuffer(privBD); 
				if (!pbwBD.equals("")) profileBW = pbwBD;
	
				request.getSession().setAttribute("utente", profileId);
				request.getSession().setAttribute("entirePrivilege", entirePrivilege);
	
				utente = profileId;
				cognome = "CognomeBD ";
				nome = "NomeBD";
			}
			else {
				profileId="U015048"; //ROBERTO BERBOTTO
				urlGetProfile="http://servizibe-swp20.sede.corp.sanpaoloimi.com:7024/scriptSwp20/swp2/";
	//			entirePrivilege = new StringBuffer("YA8TUB;");
				entirePrivilege = new StringBuffer("YA8TBA;");
				request.getSession().setAttribute("entirePrivilege", entirePrivilege);
				utente = profileId;
				request.getSession().setAttribute("utente", utente);
				profileBW = "YAR6BP9999999999;YAR6VA9999999999;BHBW00189999999999;BHBW00309999999999;YAR6BB9999999999;";
				cognome = "CognomeLoc";
				nome = "NomeLoc";
			}
		}
		else {
			if (profileId == null || profileId.isEmpty()) log.error("Header profileId is null");
			else if (urlGetProfile == null || urlGetProfile.isEmpty()) log.error("Header urlGetProfile is null");
			else {
				int i = profileId.indexOf("\\");
				if (i != -1) profileId = profileId.substring(i + 1);
				log.info("profileId = " + profileId);
				utente = profileId;
				request.getSession().setAttribute("utente", utente);
	
				String xmlUrl = urlGetProfile + "/GetProfilazione?userId=" + profileId + "&siteName=" + siteName;
				log.info("xmlUrl = " + xmlUrl);
				try {
					URL website = new URL(xmlUrl);
					URLConnection connection = website.openConnection();
					BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
	
					String inputLine;
					StringBuffer buff = new StringBuffer();
					while ((inputLine = in.readLine()) != null) {
						buff.append(inputLine);
					}
					in.close();
	
					String swpXML = buff.toString();
					log.info(" GETPROFILAZIONE RESPONSE " + buff.toString());
	
					//String swpXML = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><ProfilazioneType><responseStatus><retCode>000</retCode><retDesc>Estrazione avvenuta con successo</retDesc><message>Risultato recuperato dalla cache</message><numeroElementi>29</numeroElementi></responseStatus><profilazione><anagraficaUtente/><decodificaProfili/><userinfo utente=\"U015048\"><userprofile azienda=\"01\" uo=\"04636\" societa=\"01\"><abilitazioni><abilitazione codice=\"T08T00\"/><abilitazione codice=\"XASZRD\"/><abilitazione codice=\"YA4PXM\"/><abilitazione codice=\"YA8SXI\"/><abilitazione codice=\"YA8T10\"/><abilitazione codice=\"YA8T12\"/><abilitazione codice=\"YA8T14\"/><abilitazione codice=\"YA8TA5\"/><abilitazione codice=\"YA8TAA\"/><abilitazione codice=\"YA8TAB\"/><abilitazione codice=\"YA8TG0\"/><abilitazione codice=\"YA8TO1\"/><abilitazione codice=\"YA8TP1\"/><abilitazione codice=\"YA8TRO\"/><abilitazione codice=\"YA8TRT\"/><abilitazione codice=\"YA8TVZ\"/><abilitazione codice=\"YAF0XI\"/><abilitazione codice=\"YAF0XM\"/><abilitazione codice=\"YAIYCP\"/><abilitazione codice=\"YAIYIC\"/><abilitazione codice=\"YAMCAL\"/><abilitazione codice=\"YAMCCA\"/><abilitazione codice=\"YAMCD1\"/><abilitazione codice=\"YAMCE4\"/><abilitazione codice=\"YAMCNA\"/><abilitazione codice=\"YAR6B0\"/><abilitazione codice=\"YAR6BM\"/></abilitazioni><profili><profilo codice=\"000000\"/></profili><abilitazioniProfilo name=\"000000\"><abilitazione codice=\"Z0\"/></abilitazioniProfilo></userprofile></userinfo></profilazione></ProfilazioneType>";
					for (; swpXML.indexOf("abilitazione codice") > 0;) {
						String sAppo = swpXML.substring(swpXML.indexOf("abilitazione codice") + 21, swpXML.length());
						sAppo = sAppo.substring(0, sAppo.indexOf("\""));
						swpXML = swpXML.substring(swpXML.indexOf("abilitazione codice") + 21 + sAppo.length() + 1, swpXML.length());
						entirePrivilege.append(sAppo + ";");
					}
					request.getSession().setAttribute("entirePrivilege", entirePrivilege);
	
					String xmlUrlAna = urlGetProfile + "/GetAnagraficaUtente?userId=" + profileId + "&siteName=" + siteName;
					log.info("xmlUrlAna = " + xmlUrlAna);
					URL websiteAna = new URL(xmlUrlAna);
					URLConnection connectionAna = websiteAna.openConnection();
					BufferedReader inAna = new BufferedReader(new InputStreamReader(connectionAna.getInputStream()));
	
					String inputLineAna;
					StringBuffer buffAna = new StringBuffer();
					while ((inputLineAna = inAna.readLine()) != null) {
						buffAna.append(inputLineAna);
					}
					inAna.close();
	
					String swpXMLAna = buffAna.toString();
					log.info(" GETANAGRAFICA RESPONSE " + buffAna.toString());
	
					//String swpXMLAna = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><AnagraficaUtenteType><responseStatus><retCode>000</retCode><retDesc>Estrazione avvenuta con successo</retDesc><message>Risultato recuperato dalla cache</message><numeroElementi>1</numeroElementi></responseStatus><datiAnagrafici><userId>U015048</userId><nome>ROBERTO</nome><cognome>BERBOTTO</cognome><codiceFiscale>BRBRRT66E08L219S</codiceFiscale><azienda>01</azienda><descAzienda>INTESA SANPAOLO S.P.A.</descAzienda><uo>04636</uo><descUo>DC PO SISTEMI INFORMATIVI DEL PERSONALE</descUo><societa>01</societa><descSocieta>INTESA SANPAOLO S.P.A.</descSocieta></datiAnagrafici></AnagraficaUtenteType>";
	
					if (swpXMLAna.indexOf("cognome") > 0) cognome = swpXMLAna.substring(swpXMLAna.indexOf("cognome") + 8, swpXMLAna.indexOf("</cognome>"));
					if (swpXMLAna.indexOf("nome") > 0) nome = swpXMLAna.substring(swpXMLAna.indexOf("nome") + 5, swpXMLAna.indexOf("</nome>"));
				}
				catch (Exception e) { e.printStackTrace(); }
			}
	
			String suffisso = "_TEST";
			if (System.getProperty("DD_GPE30_ambiente_ISP")!=null) {
				if (System.getProperty("DD_GPE30_ambiente_ISP").contains("PRODUZIONE")) suffisso = "_PROD";
				if (System.getProperty("DD_GPE30_ambiente_ISP").contains("SYSTEMTEST")) suffisso = "_TEST";		
			}
			/*
			GesConn.initConfig();
			String bwUrl = GesConn.FindProperties("URLBWGETPROFILE"+suffisso)+"?u="+profileId;
			log.info(" bwUrl " + bwUrl);
			try
			{
				StringBuilder bwBuff = readFile(bwUrl);
				profileBW = bwBuff.toString();
			}catch (Exception e) {e.printStackTrace();}
			 */
			profileBW = getStringaBW();
			utente = profileId;
	
			request.getSession().setAttribute("utente", utente);
			request.getSession().setAttribute("entirePrivilege", entirePrivilege);
	
			// profileBW = "BHBW00871000008300;BHBW00871000012100;BHBW00871000033700;BHBW00871000089200;BHBW00871000142700;BHBW00871000153000;BHBW00871000338701;BHBW00871000508700;BHBW00871000519200;BHBW00871000535700;BHBW00871000536000;BHBW00871000549800;BHBW00871000550900;BHBW00871000557500;BHBW00871000562700;BHBW00871000564200;BHBW00871000564600;BHBW00871000584200;BHBW00871001004100;BHBW00871001004300;BHBW00871001006400;BHBW00871001007000;BHBW00871001007400;BHBW00871001485500;BHBW00871001490100;BHBW00871001845400;BHBW00871001847400;BHBW00871001910000;BHBW00871001937500;BHBW00871002275000;BHBW00871002425000;BHBW00871002425400;BHBW00871002425800;BHBW00871002426200;BHBW00871003039300;BHBW00871003039700;BHBW00871003057800;BHBW00871003090000;BHBW00871003150600;BHBW00871003910000;BHBW00871005519700;YAR6N11000008300;YAR6N11000012100;YAR6N11000033700;YAR6N11000089200;YAR6N11000142700;YAR6N11000153000;YAR6N11000338701;YAR6N11000508700;YAR6N11000519200;YAR6N11000535700;YAR6N11000536000;YAR6N11000549800;YAR6N11000550900;YAR6N11000557500;YAR6N11000562700;YAR6N11000564200;YAR6N11000564600;YAR6N11000584200;YAR6N11001004100;YAR6N11001004300;YAR6N11001006400;YAR6N11001007000;YAR6N11001007400;YAR6N11001485500;YAR6N11001490100;YAR6N11001845400;YAR6N11001847400;YAR6N11001910000;YAR6N11001937500;YAR6N11002275000;YAR6N11002425000;YAR6N11002425400;YAR6N11002425800;YAR6N11002426200;YAR6N11003039300;YAR6N11003039700;YAR6N11003057800;YAR6N11003090000;YAR6N11003150600;YAR6N11003910000;YAR6N11005519700;YAR6NA1005575205;NZBW00051005575205;BHBW00151000008300;BHBW00151000012100;BHBW00151000033700;BHBW00151000089200;BHBW00151000142700;BHBW00151000153000;BHBW00151000338701;BHBW00151000508700;BHBW00151000519200;BHBW00151000535700;BHBW00151000536000;BHBW00151000549800;BHBW00151000550900;BHBW00151000557500;BHBW00151000562700;BHBW00151000564200;BHBW00151000564600;BHBW00151000584200;BHBW00151001004100;BHBW00151001004300;BHBW00151001006400;BHBW00151001007000;BHBW00151001007400;BHBW00151001485500;BHBW00151001490100;BHBW00151001845400;BHBW00151001847400;BHBW00151001910000;BHBW00151001937500;BHBW00151002275000;BHBW00151002425000;BHBW00151002425400;BHBW00151002425800;BHBW00151002426200;BHBW00151003039300;BHBW00151003039700;BHBW00151003057800;BHBW00151003090000;BHBW00151003150600;BHBW00151003910000;BHBW00151005519700;YAR6JX1000008300;YAR6JX1000012100;YAR6JX1000033700;YAR6JX1000089200;YAR6JX1000142700;YAR6JX1000153000;YAR6JX1000338701;YAR6JX1000508700;YAR6JX1000519200;YAR6JX1000535700;YAR6JX1000536000;YAR6JX1000549800;YAR6JX1000550900;YAR6JX1000557500;YAR6JX1000562700;YAR6JX1000564200;YAR6JX1000564600;YAR6JX1000584200;YAR6JX1001004100;YAR6JX1001004300;YAR6JX1001006400;YAR6JX1001007000;YAR6JX1001007400;YAR6JX1001485500;YAR6JX1001490100;YAR6JX1001845400;YAR6JX1001847400;YAR6JX1001910000;YAR6JX1001937500;YAR6JX1002275000;YAR6JX1002425000;YAR6JX1002425400;YAR6JX1002425800;YAR6JX1002426200;YAR6JX1003039300;YAR6JX1003039700;YAR6JX1003057800;YAR6JX1003090000;YAR6JX1003150600;YAR6JX1003910000;YAR6JX1005519700;";
		}
	
		log.info(" profileId " + profileId);
		log.info(" Nominativo " + cognome +" "+ nome);
	
		log.info(" entirePrivilege " + entirePrivilege.toString());
		log.info(" profileBW " + profileBW.toString());
	}

	public String getStringaBW() {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		GesConn.initConfig();
		String sOut = "";
		try {
			conn = GesConn.CreaConnessione("getStringaBW");
			ps = conn.prepareStatement("select nvl(gper0_servizi.get_stringa_bw(?),'') as stringaBW from dual");

			ps.setString(1, profileId);
			rs = ps.executeQuery();
			while (rs.next())
				sOut = Utility.rsVarchar2ToString(rs.getObject("stringaBW"));
		}
		catch (Exception e) { e.printStackTrace(); }
		finally {
			try { if (conn != null) conn.close(); }
			catch (Exception ex) { ex.printStackTrace(); }
		}
		return sOut;
	}

//	public static boolean  getAbilitazioneDaProfilo () {
//		log.info("entirePrivilege" + entirePrivilege);
//		return entirePrivilege!= null && (entirePrivilege.indexOf("YA8TUB")>=0);
//	}
	
	public static int getAbilitazioneUtente() {
		log.info("entirePrivilege" + entirePrivilege);
		
		if(entirePrivilege!= null && (entirePrivilege.indexOf("YA8TBA")>=0)) 
			return 3;
		else if(entirePrivilege!= null && (entirePrivilege.indexOf("YA8TBB")>=0)) 
			return 1;
		else if(entirePrivilege!= null && (entirePrivilege.indexOf("YA8TBC")>=0)) 
			return 2;
		else 
			return 0;
	}
}
