package it.eng.utility;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.net.URLConnection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.zip.CRC32;
import java.util.zip.Checksum;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

public class Utility {
	public static final String DATE_PATTERN_DATE_HOUR_SEC_FILENAME = "yyyyMMdd_HHmmss";
	public static final String DATE_PATTERN_DATE_HOUR_SEC_MILLIS = "dd/MM/yyyy HH:mm:ss:S";
	public static final String DATE_PATTERN_DATE_HOUR_SEC = "dd/MM/yyyy HH:mm:ss";
	public static final String DATE_PATTERN_HOUR = "dd/MM/yyyy HH:mm";
	public static final String DATE_PATTERN = "dd/MM/yyyy";
	public static final String ISO_DATE_PATTERN = "yyyy-MM-dd";

	public static final String DATE_PATTERN_ISO_8601 = "yyyy-MM-dd'T'HH:mm:ss.SSS";

	public static String dateHourToString(Date d) {
		if (d == null)
			return null;
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_PATTERN_HOUR);
		return sdf.format(d);
	}

	public static String dateHourMinSecToString(Date d) {
		if (d == null)
			return null;
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_PATTERN_DATE_HOUR_SEC);
		return sdf.format(d);
	}

	public static String dateHourMinSecMillisToString(Date d) {
		if (d == null)
			return null;
		SimpleDateFormat sdf = new SimpleDateFormat(
				DATE_PATTERN_DATE_HOUR_SEC_MILLIS);
		return sdf.format(d);
	}

	public static String dateHourMinSecMillisToFilenameString(Date d) {
		if (d == null)
			return null;
		SimpleDateFormat sdf = new SimpleDateFormat(
				DATE_PATTERN_DATE_HOUR_SEC_FILENAME);
		return sdf.format(d);
	}

	public static String dateToString(Date d) {
		if (d == null)
			return null;
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_PATTERN);
		return sdf.format(d);
	}

	public static String dateToString8601(Date d) {
		if (d == null)
			return null;
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_PATTERN_ISO_8601);
		return sdf.format(d);
	}

	public static Date stringToDate8601(String s) throws ParseException {
		if (s == null)
			return null;
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_PATTERN_ISO_8601);
		return sdf.parse(s);
	}

	public static String dateToStringISOFormat(Date d) {
		if (d == null)
			return null;
		SimpleDateFormat sdf = new SimpleDateFormat(ISO_DATE_PATTERN);
		return sdf.format(d);
	}

	public static Date stringToDate(String s) throws ParseException {
		if (s == null)
			return null;
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_PATTERN);
		return sdf.parse(s);
	}

	public static Date stringToDateHour(String s) throws ParseException {
		if (s == null)
			return null;
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_PATTERN_HOUR);
		return sdf.parse(s);
	}

	public static Date stringToFormattedDate(String s, String sFormato)
			throws ParseException {
		if (s == null)
			return null;
		SimpleDateFormat sdf = new SimpleDateFormat(sFormato);
		return sdf.parse(s);
	}

	public static Date stringToDateHourMinSec(String s) throws ParseException {
		if (s == null)
			return null;
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_PATTERN_DATE_HOUR_SEC);
		return sdf.parse(s);
	}

	public static boolean isDataValida(String inputString) {
		SimpleDateFormat format = new java.text.SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");
		try {
			format.parse(inputString);
			return true;
		} catch (ParseException e) {
			return false;
		}
	}

	public static String fillZero(int num) {
		if (num < 10)
			return "0" + String.valueOf(num);
		else
			return String.valueOf(num);
	}

	public static boolean checkCaratteriJolly(String str) {
		String caratteriOK = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		boolean retValue = true;
		for (int i = 0; i < str.length() - 1; i++) {
			String subStr = str.substring(i, i + 1);
			if (caratteriOK.indexOf(subStr) < 0) {
				retValue = false;
				break;
			}
		}
		return retValue;
	}

	public static BigDecimal stringToNumber(String s) throws SQLException {
		BigDecimal bd = new BigDecimal(s);
		return new BigDecimal(bd.doubleValue());
	}

	public static BigDecimal stringToSignedNumber(String s, String sign)
			throws SQLException {
		BigDecimal bd = null;
		if (sign.equalsIgnoreCase("-"))
			bd = new BigDecimal("-" + s);
		else
			bd = new BigDecimal(s);
		bd = bd.setScale(2, BigDecimal.ROUND_FLOOR);
		return bd;
	}

	public static double stringToDouble(String s) {
		BigDecimal bd = new BigDecimal(s);
		bd = bd.setScale(2, BigDecimal.ROUND_FLOOR);
		return bd.doubleValue();
	}

	public static String doubleToString(double d) {
		return String.valueOf(d).replace(".", ",");
	}

	public static int annoCorrente() {
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(new Date());
		return gc.get(GregorianCalendar.YEAR);
	}

	public static int meseCorrente() {
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(new Date());
		return gc.get(GregorianCalendar.MONTH) + 1;
	}

	public static String bigDecimalToString(BigDecimal d) {
		String s = null;
		if (d == null)
			return null;
		d = d.setScale(2, BigDecimal.ROUND_HALF_UP);
		s = d.toString();
		if (s.indexOf(".") < 0)
			s += ".00";
		return s;
	}

	public static String bigDecimalToStringExcel(BigDecimal d) {
		String s = null;
		if (d == null)
			return null;
		s = d.toString();
		if (s.indexOf(".") < 0)
			s += ".00";
		s = s.replace(".", ",");
		return s;
	}

	public static String bigDecimalToFormattedString(BigDecimal d) {
		String s = null;
		boolean neg = false;
		if (d == null)
			return null;
		if (d.doubleValue() < 0) {
			neg = true;
			d = new BigDecimal(d.toString().substring(1));
		}
		d.setScale(2, BigDecimal.ROUND_HALF_UP);
		s = d.toString();
		if (s.indexOf(".") < 0)
			s += ".00";
		s = s.replace(".", ",");
		int cifreInt = s.indexOf(",");
		int a = cifreInt % 3;
		String sOut = "";
		if (a > 0)
			sOut = s.substring(0, a) + ".";
		StringBuffer ssss = new StringBuffer(sOut);
		for (int i = 0; i < cifreInt / 3; i++)
			ssss.append(s.substring(a + i * 3, a + (i + 1) * 3) + ".");
		sOut = ssss.toString();
		sOut = sOut.substring(0, sOut.length() - 1);
		sOut += s.substring(s.indexOf(","), s.length());
		if (sOut.indexOf(",") == 0) {
			sOut = "0" + sOut;
		}
		if (neg) {
			sOut = "-" + sOut;
		}
		return sOut;
	}

	public static String bigDecimalToFormattedStringNoDecimali(BigDecimal d) {
		String s = null;
		boolean neg = false;
		if (d == null)
			return null;
		if (d.doubleValue() < 0) {
			neg = true;
			d = new BigDecimal(d.toString().substring(1));
		}
		d.setScale(2, BigDecimal.ROUND_HALF_UP);
		s = d.toString();
		int cifreInt = s.length();
		int a = cifreInt % 3;
		String sOut = "";
		if (a > 0)
			sOut = s.substring(0, a) + ".";
		StringBuffer ssss = new StringBuffer(sOut);
		for (int i = 0; i < cifreInt / 3; i++)
			ssss.append(s.substring(a + i * 3, a + (i + 1) * 3) + ".");
		sOut = ssss.toString();
		sOut = sOut.substring(0, sOut.length() - 1);
		// sOut += s.substring(s.indexOf(","),s.length());
		// if (sOut.indexOf(",")==0) {sOut ="0" + sOut;}
		if (neg) {
			sOut = "-" + sOut;
		}
		return sOut;
	}

	public static String bigDecimalToStringNoZero(BigDecimal d) {
		if (d == null)
			return null;
		d.setScale(2, BigDecimal.ROUND_HALF_UP);
		return d.toString();
	}

	public static String bigDecimalToStringNoDecimali(BigDecimal d) {
		if (d == null)
			return null;
		d.setScale(0, BigDecimal.ROUND_HALF_UP);
		return d.toString();
	}

	public static BigDecimal bigDecimalSum(BigDecimal bd1, BigDecimal bd2) {
		if (bd1 == null)
			bd1 = new BigDecimal("0");
		if (bd2 == null)
			bd2 = new BigDecimal("0");
		BigDecimal result = bd1.add(bd2).setScale(2, RoundingMode.HALF_DOWN);
		result = result.setScale(2, RoundingMode.HALF_DOWN);
		return result;
	}

	public static BigDecimal bigDecimalSubtract(BigDecimal bd1, BigDecimal bd2) {
		if (bd1 == null)
			bd1 = new BigDecimal("0");
		if (bd2 == null)
			bd2 = new BigDecimal("0");
		BigDecimal result = bd1.subtract(bd2).setScale(2,
				RoundingMode.HALF_DOWN);
		result = result.setScale(2, RoundingMode.HALF_DOWN);
		return result;
	}

	public static Date sommaMesi(Date dIn, int mesi) {
		Date dOut = dIn;
		if (mesi > 0) {
			GregorianCalendar gc = new GregorianCalendar();
			gc.setTime(dIn);
			gc.add(GregorianCalendar.MONTH, mesi);
			dOut = gc.getTime();
		}
		return dOut;
	}

	public static Date sommaGiorni(Date date, int days) {

		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DAY_OF_MONTH, days);
		return cal.getTime();
	}

	public static boolean StringaSoloNumeri(String stringa) {
		boolean bRet = true;
		String okChar = "0123456789";
		for (int i = 0; i < stringa.length(); i++)
			if (okChar.indexOf(stringa.substring(i, i + 1)) < 0) {
				bRet = false;
			}
		return bRet;
	}

	public static void byteToFile(byte[] file, String name) {
		try {
			FileOutputStream fileOuputStream = new FileOutputStream(name);
			fileOuputStream.write(file);
			fileOuputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static boolean contieneStringa(ArrayList<String> sElenco, String desc) {
		boolean bContiene = false;
		for (Iterator<String> iterator = sElenco.iterator(); iterator.hasNext();) {
			String string = (String) iterator.next();
			if (string != null && string.equalsIgnoreCase(desc)) {
				bContiene = true;
				break;
			}
		}
		return bContiene;
	}

	public static String rtrim(String s) {
		int i = s.length() - 1;
		while (i >= 0 && Character.isWhitespace(s.charAt(i))) {
			i--;
		}
		return s.substring(0, i + 1);
	}

	public static String ltrim(String s) {
		int i = 0;
		while (i < s.length() && Character.isWhitespace(s.charAt(i))) {
			i++;
		}
		return s.substring(i);
	}

	public static BigDecimal formattedStringToBigDecimal(String s) {
		BigDecimal outBD = null;
		if (s != null && !s.equals("")) {
			s = s.replace(".", "");
			s = s.replace(",", ".");
			try {
				outBD = new BigDecimal(s);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		return outBD;
	}

	public static String ultimoGiornoDelMese(int month, int year) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(year, month - 1, 1);
		calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
		Date date = calendar.getTime();
		DateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");
		return DATE_FORMAT.format(date);
	}

	private static Map<String, String> mesiMap = null;

	public static Map<String, String> getMesiMap() {

		if (mesiMap == null) {
			mesiMap = new HashMap<String, String>();
			mesiMap.put("01", "Gennaio");
			mesiMap.put("02", "Febbraio");
			mesiMap.put("03", "Marzo");
			mesiMap.put("04", "Aprile");
			mesiMap.put("05", "Maggio");
			mesiMap.put("06", "Giugno");
			mesiMap.put("07", "Luglio");
			mesiMap.put("08", "Agosto");
			mesiMap.put("09", "Settembre");
			mesiMap.put("10", "Ottobre");
			mesiMap.put("11", "Novembre");
			mesiMap.put("12", "Dicembre");
		}
		return mesiMap;

	}

	public static StringBuilder readFile(String urlFile) throws Exception {
		StringBuilder buff = new StringBuilder();
		URL url = new URL(urlFile);
		URLConnection conn = url.openConnection();

		// open the stream and put it into BufferedReader
		BufferedReader br = new BufferedReader(new InputStreamReader(
				conn.getInputStream()));

		String inputLine;

		if (((inputLine = br.readLine()) != null))
			buff.append(inputLine);
		while ((inputLine = br.readLine()) != null) {
			buff.append("\n");
			buff.append(inputLine);
		}

		br.close();
		return buff;

	}

	public static String StringToVarchar2(String s) {
		if (s != null)
			return "'" + s + "'";
		else
			return "null";
	}

	public static String rsVarchar2ToString(Object o) {
		if (o == null)
			return "";
		else
			return o.toString();
	}

	public static Date rsTimeStampToDate(Timestamp ts) {
		if (ts == null)
			return null;
		else
			return new Date(ts.getTime());
	}

	public static BigDecimal rsNumberToBigDecimal(Object o) {
		if (o == null)
			return new BigDecimal(0);
		else
			return (BigDecimal) o;
	}

	public static boolean dataCompresaTra(String dataX, String dataDa,
			String dataA) {
		boolean retValue = false;
		Date dtX;
		Date dtDa;
		Date dtA;
		try {
			dtX = stringToDate(dataX);
			dtDa = stringToDate(dataDa);
			dtA = stringToDate(dataA);
			retValue = (dtDa.compareTo(dtX) * dtX.compareTo(dtA) > 0)
					|| dtX.compareTo(dtA) == 0 || dtDa.compareTo(dtX) == 0;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public static byte[] compressStringToByte(String text) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			OutputStream out = new DeflaterOutputStream(baos);
			out.write(text.getBytes("ISO-8859-1"));
			out.close();
		} catch (IOException e) {
			throw new AssertionError(e);
		}
		return baos.toByteArray();
	}

	public static String decompressByeToString(byte[] bytes) {
		InputStream in = new InflaterInputStream(
				new ByteArrayInputStream(bytes));
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			byte[] buffer = new byte[8192];
			int len;
			while ((len = in.read(buffer)) > 0)
				baos.write(buffer, 0, len);
			return new String(baos.toByteArray(), "ISO-8859-1");
		} catch (IOException e) {
			throw new AssertionError(e);
		}
	}

	public static long calcolaCrc32perStringa(String ss) {
		String sTemp = ss;
		byte bytes[] = sTemp.getBytes();
		Checksum checksum = new CRC32();
		checksum.update(bytes, 0, bytes.length);
		return checksum.getValue();
	}

	public static String quote(String string) {
		if (string == null || string.length() == 0) {
			return "";
		}

		char c = 0;
		int i;
		int len = string.length();
		StringBuilder sb = new StringBuilder(len + 4);
		String t;

		for (i = 0; i < len; i += 1) {
			c = string.charAt(i);
			switch (c) {
			case '\\':
				sb.append('\\');
				sb.append('\\');
				break;
			case '"':
				sb.append('\\');
				sb.append(c);
				break;
			case '/':
				sb.append('\\');
				sb.append(c);
				break;
			case '\b':
				sb.append("\\b");
				break;
			case '\t':
				sb.append("\\t");
				break;
			case '\n':
				sb.append("\\n");
				break;
			case '\f':
				sb.append("\\f");
				break;
			case '\r':
				sb.append("\\r");
				break;
			default:
				if (c < ' ') {
					t = "000" + Integer.toHexString(c);
					sb.append("\\u" + t.substring(t.length() - 4));
				} else
					sb.append(c);
			}
		}
		return sb.toString();
	}

	public static boolean parseISOData(String date) {
		boolean retValue = false;

		if (date == null)
			return retValue;

		try {
			SimpleDateFormat sdf = new SimpleDateFormat(ISO_DATE_PATTERN);
			sdf.parse(date);
			retValue = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}

	public static String accoda(String sStart, String sAppend) {
		StringBuilder sOut = new StringBuilder(sStart != null ? sStart : "");
		if (sAppend != null)
			sOut.append(sAppend);
		return sOut.toString();

	}

	public static String fillStr(String sIn, String fillStr, int dim) {
		String sOut = sIn;
		for (int k = sIn.length(); k < dim; k++)
			sOut = accoda(sOut, fillStr);
		return sOut;
	}

	public static String bigDecimalToString4Decimali(BigDecimal d) {
		String s = null;
		if (d == null)
			return null;
		d = d.setScale(4, BigDecimal.ROUND_HALF_UP);
		s = d.toString();
		if (s.indexOf(".") < 0)
			s += ".0000";
		return s;
	}

	public static int calcolaEta(String dataNascita) {
		int age = 0;
		try {
			Calendar now = Calendar.getInstance();
			Calendar dob = Calendar.getInstance();
			dob.setTime(Utility.stringToDate(dataNascita));
			if (dob.after(now)) {
				throw new IllegalArgumentException(
						"Can't be born in the future");
			}
			int year1 = now.get(Calendar.YEAR);
			int year2 = dob.get(Calendar.YEAR);
			age = year1 - year2;
			int month1 = now.get(Calendar.MONTH);
			int month2 = dob.get(Calendar.MONTH);
			if (month2 > month1)
				age--;
			else if (month1 == month2) {
				int day1 = now.get(Calendar.DAY_OF_MONTH);
				int day2 = dob.get(Calendar.DAY_OF_MONTH);
				if (day2 > day1)
					age--;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return age;
	}
	
	public static boolean isObjectEmpty(Object value) {

		  if (value == null) {
		   return true;
		  }
		  if (value.getClass().getName().equals("java.lang.String") && value.toString().trim().equals("")) {
		   return true;
		  }

		  return false;
		 }
	
	public static byte[] readContentIntoByteArray(File file)
	   {
	      FileInputStream fileInputStream = null;
	      byte[] bFile = new byte[(int) file.length()];
	      try
	      {
	         fileInputStream = new FileInputStream(file);
	         fileInputStream.read(bFile);
	         fileInputStream.close();
	      }
	      catch (Exception e)
	      {
	         e.printStackTrace();
	      }
	      return bFile;
	   }
}