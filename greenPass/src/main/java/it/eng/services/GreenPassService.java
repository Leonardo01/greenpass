package it.eng.services;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

import it.eng.services.bodies.GreenPassBody;
import it.eng.services.bodies.dao.dto.CrudRequest;
import it.eng.services.bodies.dao.dto.FilterRequest;
import it.eng.services.bodies.dao.dto.Greenpass;
import it.eng.services.bodies.dao.dto.InitFilterResponse;

@Path("/greenpass")
public class GreenPassService {

	private static Logger log = Logger.getLogger(GreenPassService.class);

	// ******************//
	// *** SALVA, CONFERMA, CANCELLA *** //
	// ******************//

	@POST
	@Path("/crud")
	@Produces("application/json; charset=UTF-8")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response crud(CrudRequest cr) {
		String response = GreenPassBody.crud(cr);
		return Response.ok(new Gson().toJson(response), MediaType.APPLICATION_JSON).build();
	}

	// ******************//
	// *** SHOW FILTERS *** //
	// ******************//

	@GET
	@Path("/initFilter")
	@Produces("application/json; charset=UTF-8")
	public Response initFilter() {
		InitFilterResponse response = GreenPassBody.initFiler();
		return Response.ok(new Gson().toJson(response), MediaType.APPLICATION_JSON).build();
	}

	// ******************//
	// *** CERCA *** //
	// ******************//

	@POST
	@Path("/filter")
	@Produces("application/json; charset=UTF-8")
	public Response filter(FilterRequest request) {
		List<Greenpass> response = GreenPassBody.filter(request);
		return Response.ok(new Gson().toJson(response), MediaType.APPLICATION_JSON).build();
	}
}
