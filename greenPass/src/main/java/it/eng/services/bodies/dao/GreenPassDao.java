package it.eng.services.bodies.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import it.eng.services.bodies.dao.dto.CrudRequest;
import it.eng.services.bodies.dao.dto.FilterRequest;
import it.eng.services.bodies.dao.dto.Greenpass;
import it.eng.services.bodies.dao.dto.InitFilterResponse;

public class GreenPassDao extends BaseDao  {
	
	private static final long serialVersionUID = 1L;
	private Logger log = Logger.getLogger(GreenPassDao.class);
	private PreparedStatement pstmt;
	private CallableStatement cstmt;
	private ResultSet rs;
	private Connection conn;
	
	
	public GreenPassDao(Connection conn) throws Exception {
		this.conn=conn;
	}
	
	public InitFilterResponse initFilter()  {
		InitFilterResponse response = new InitFilterResponse();
		Set<String> distaccamentoSet = new LinkedHashSet<String>();
		Set<String> indirizzoDistaccamentoSet = new LinkedHashSet<String>();
		String query = "SELECT DISTINCT distaccamento, indirizzo_distaccamento, societa, filiale, indirizzo_filiale FROM tb_greenpass";
		try {
			pstmt = conn.prepareStatement(query);
            rs = pstmt.executeQuery();
            while(rs.next()) {
            	if(rs.getString(1) != null) {
            	distaccamentoSet.add(rs.getString(1));
            	}
            	if(rs.getString(1) != null) {
            	indirizzoDistaccamentoSet.add(rs.getString(2));
            	}
            	if(rs.getString(1) != null) {
            	response.setSocieta(rs.getString(3));
            	}
            	if(rs.getString(1) != null) {
            	response.setFiliale(rs.getString(4));
            	}
            	if(rs.getString(1) != null) {
            	response.setIndirizzoFiliale(rs.getString(5));
            	}
                }
            response.setDistaccamento(distaccamentoSet);
            response.setIndirizzoDistaccamento(indirizzoDistaccamentoSet);
            pstmt.close();
            rs.close();
            conn.close();
		} catch (SQLException e) {
			System.out.println(e);
		}
		return response;
	}
	
	public List<Greenpass> filter(FilterRequest request){
		List<Greenpass> greenpassList = new ArrayList<Greenpass>();
		String query = "SELECT data_verifica, ora_verifica, nominativo, n_controllati_int, n_controllati_est, id_greenpass FROM tb_greenpass "
					+ "WHERE societa = ? AND filiale = ? AND indirizzo_filiale = ? "
					+ "AND distaccamento = ? AND indirizzo_distaccamento = ? AND flag_conferma = 0";
		try {
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, request.getSocieta());
			pstmt.setString(2, request.getFiliale());
			pstmt.setString(3, request.getIndirizzoFiliale());
			pstmt.setString(4, request.getDistaccamento());
			pstmt.setString(5, request.getIndirizzoDistaccamento());
            rs = pstmt.executeQuery();
            while(rs.next()) {
            	Greenpass greenpass = new Greenpass();
            	greenpass.setId(rs.getLong(6));
            	greenpass.setDataVerifica(rs.getDate(1).toString());
            	greenpass.setOraVerifica(rs.getTimestamp(2).toLocalDateTime().format(DateTimeFormatter.ofPattern("HH:mm")));
            	greenpass.setNominativoIncaricato(rs.getString(3));
            	greenpass.setnSoggInt(rs.getInt(4));
            	greenpass.setnSoggEst(rs.getInt(5));
            	greenpassList.add(greenpass);
            }
            pstmt.close();
            rs.close();
            conn.close();
		} catch (SQLException e) {
			System.out.println(e);
		}
		return greenpassList;
	}
	
	public String save(CrudRequest cr) {
		List<Greenpass> greenpassList = cr.getGreenpassList();
		String queryCall = "{CALL SAVE_GREENPASS(?, ? ,? ,?, ?, ?)}";
		String response = "";
		try {
			for (int i=0; i<greenpassList.size(); i++) {
				Greenpass greenpass = new Greenpass();
				greenpass = greenpassList.get(i);
					cstmt = conn.prepareCall(queryCall);
					cstmt.setLong(1, greenpass.getId());
					cstmt.setDate(2, Date.valueOf(LocalDate.parse(greenpass.getDataVerifica())));
					cstmt.setTime(3, Time.valueOf(LocalTime.parse(greenpass.getOraVerifica())));
					cstmt.setString(4, greenpass.getNominativoIncaricato());
					cstmt.setInt(5, greenpass.getnSoggInt());
					cstmt.setInt(6, greenpass.getnSoggEst());
					cstmt.execute();
			}
			cstmt.close();
			conn.close();
			response = "Greenpass salvati!";
		} catch (SQLException e) {
			System.out.println(e);
			response = "Errore!";
		}
		return response;
	}
	
	public String send(CrudRequest cr) {
		List<Greenpass> greenpassList = cr.getGreenpassList();
		String query = "UPDATE tb_greenpass SET flag_conferma = 1 WHERE id_greenpass = ?";
		String response = "";
		try {
			for (int i=0; i<greenpassList.size(); i++) {
				Greenpass greenpass = new Greenpass();
				greenpass = greenpassList.get(i);
				pstmt = conn.prepareStatement(query);
				pstmt.setLong(1, greenpass.getId());
				pstmt.execute();
			}
			pstmt.close();
			conn.close();
			response = "Greenpass inviati!!";
		} catch (SQLException e) {
			System.out.println(e);
			response = "Errore!";
		}
		return response;
	}
	
	public String remove(CrudRequest cr) {
		List<Greenpass> greenpassList = cr.getGreenpassList();
		String query = "DELETE FROM tb_greenpass WHERE id_greenpass = ?";
		String response = "";
		try {
			for (int i=0; i<greenpassList.size(); i++) {
				Greenpass greenpass = new Greenpass();
				greenpass = greenpassList.get(i);
				pstmt = conn.prepareStatement(query);
				pstmt.setLong(1, greenpass.getId());
				pstmt.execute();
			}
			pstmt.close();
			conn.close();
			response = "Greenpass Cancellati!";
		} catch (SQLException e) {
			System.out.println(e);
			response = "Errore!";
		}
		return response;
	}
}
