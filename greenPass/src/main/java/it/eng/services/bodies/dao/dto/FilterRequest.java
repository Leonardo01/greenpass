package it.eng.services.bodies.dao.dto;

public class FilterRequest {
	
	private String societa;
	private String filiale;
	private String indirizzoFiliale;
	private String distaccamento;
	private String indirizzoDistaccamento;
	
	public FilterRequest() {}
	
	public String getSocieta() {
		return societa;
	}
	public void setSocieta(String societa) {
		this.societa = societa;
	}
	public String getFiliale() {
		return filiale;
	}
	public void setFiliale(String filiale) {
		this.filiale = filiale;
	}
	public String getIndirizzoFiliale() {
		return indirizzoFiliale;
	}
	public void setIndirizzoFiliale(String indirizzoFiliale) {
		this.indirizzoFiliale = indirizzoFiliale;
	}

	public String getDistaccamento() {
		return distaccamento;
	}

	public void setDistaccamento(String distaccamento) {
		this.distaccamento = distaccamento;
	}

	public String getIndirizzoDistaccamento() {
		return indirizzoDistaccamento;
	}

	public void setIndirizzoDistaccamento(String indirizzoDistaccamento) {
		this.indirizzoDistaccamento = indirizzoDistaccamento;
	}

}
