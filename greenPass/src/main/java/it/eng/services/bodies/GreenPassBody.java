package it.eng.services.bodies;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import it.eng.services.bodies.dao.GreenPassDao;
import it.eng.services.bodies.dao.conn.GesConn;
import it.eng.services.bodies.dao.dto.CrudRequest;
import it.eng.services.bodies.dao.dto.FilterRequest;
import it.eng.services.bodies.dao.dto.Greenpass;
import it.eng.services.bodies.dao.dto.InitFilterResponse;

public class GreenPassBody {

	private static Logger log = Logger.getLogger(GreenPassBody.class);

	public static String crud(CrudRequest cr) {

		GesConn.initConfig();
		Connection conn = null;
		String response = "Esito buono";
		try {
			conn = GesConn.CreaConnessione("checkCandidato");
			GreenPassDao dao = new GreenPassDao(conn);

			switch(cr.getFlgInviato()) {
			case "SALVA":
							dao.save(cr);
				break;
			case "INVIA":
							dao.send(cr);
				break;
			case "ELIMINA":
							dao.remove(cr);
				break;
			}
		}
		catch(Exception ex) { log.error(ex.getMessage()); }
		finally { try { if(conn!= null) {conn.close();}} catch (Exception ex) {log.error(ex.getMessage());} }

		return response;
	}

	public static InitFilterResponse initFiler() {
		GesConn.initConfig();
		Connection conn = null;
		InitFilterResponse response = new InitFilterResponse();
		
		try {
			conn = GesConn.CreaConnessione("checkCandidato");
			GreenPassDao dao = new GreenPassDao(conn);

			response = dao.initFilter();

		}
		catch(Exception ex) { log.error(ex.getMessage()); }
		finally { try { if(conn!= null) {conn.close();}} catch (Exception ex) {log.error(ex.getMessage());} }

		return response;
	}

	public static List<Greenpass> filter(FilterRequest request) {
		GesConn.initConfig();
		Connection conn = null;
		List<Greenpass> response = new ArrayList<Greenpass>(); 
		
		try {
			conn = GesConn.CreaConnessione("checkCandidato");
			GreenPassDao dao = new GreenPassDao(conn);
			
			response = dao.filter(request);

		}
		catch(Exception ex) { log.error(ex.getMessage()); }
		finally { try { if(conn!= null) {conn.close();}} catch (Exception ex) {log.error(ex.getMessage());} }

		return response;
	}
}
