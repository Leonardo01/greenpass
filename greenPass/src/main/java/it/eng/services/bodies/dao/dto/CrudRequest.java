package it.eng.services.bodies.dao.dto;

import java.util.List;

public class CrudRequest {
	
	private List<Greenpass> greenpassList;
	private String flgInviato;
	
	public CrudRequest() {}
	
	public List<Greenpass> getGreenpassList() {
		return greenpassList;
	}
	
	public void setGreenpassList(List<Greenpass> greenpassList) {
		this.greenpassList = greenpassList;
	}
	
	public String getFlgInviato() {
		return flgInviato;
	}
	
	public void setFlgInviato(String flgInviato) {
		this.flgInviato = flgInviato;
	}
}
