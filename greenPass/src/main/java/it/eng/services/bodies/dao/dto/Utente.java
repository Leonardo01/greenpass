package it.eng.services.bodies.dao.dto;

public class Utente {
	String nome;
	String cognome;
	String codice;
	int ruolo;
	
	
	public Utente(String nome, String cognome, String codice, int ruolo) {
		super();
		this.nome = nome;
		this.cognome = cognome;
		this.codice = codice;
		this.ruolo = ruolo;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public String getCodice() {
		return codice;
	}
	public void setCodice(String codice) {
		this.codice = codice;
	}
	public int getRuolo() {
		return ruolo;
	}
	public void setRuolo(int ruolo) {
		this.ruolo = ruolo;
	}
	
}
