package it.eng.services.bodies.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import oracle.jdbc.OracleCallableStatement;

public class BaseDao implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Logger log = Logger.getLogger(BaseDao.class);

	protected DataSource ds;
	protected boolean bDatasource;
	protected boolean autoClose = true;
	protected Connection pConn;

	public BaseDao() {
	}

	public BaseDao(Connection con) throws Exception {
		log.info("BaseDao: " + con);
		this.pConn = con;
	}

	protected PreparedStatement getPreparedStatementFORWARDANDBACK(
			String statement) throws Exception {
		PreparedStatement stmt = null;
		try {
			stmt = this.pConn.prepareStatement(statement,
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
		} catch (SQLException ex) {
			log.error("getPreparedStatementFORWARDANDBACK: ", ex);
			throw ex;
		}
		return stmt;
	}

	protected OracleCallableStatement getOracleStatement(String statement)
			throws Exception {
		OracleCallableStatement stmt = null;
		try {
			stmt = (OracleCallableStatement) this.pConn.prepareCall(statement);
		} catch (SQLException ex) {
			log.error("getOracleStatement: ", ex);
			throw ex;
		}
		return stmt;
	}

}
