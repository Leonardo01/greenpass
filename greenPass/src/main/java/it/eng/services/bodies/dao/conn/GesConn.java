package it.eng.services.bodies.dao.conn;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import org.apache.log4j.Logger;
import javax.sql.*;
import javax.naming.*;

public class GesConn {
	private static boolean propsLoaded;
	private static Properties props;
	private static String dbdriver;
	private static String stringconnection;
	public static String configDir;
	public static String logDir;

	public static String suffisso = "_LOC";

	public GesConn() {
	}

	public static void initConfig() {
		initConfig("greenPass");
	}

	public static void initConfig(String appName) {
		Logger log = Logger.getLogger(GesConn.class);
		log.info("Entrato in initConfig");
		if (!propsLoaded) {
			propsLoaded = true;

			props = new Properties();
			configDir = System.getProperty("configfile.path");
			if (configDir == null) {
				configDir = GesConn.class.getResource("/").getPath();
			} else
				configDir = configDir + appName + File.separatorChar;

			try {
				props.load(new FileInputStream("C:\\Users\\User\\Documents\\GreenPass\\greenPass\\src\\Resources.properties"));
			} catch (IOException ex) {
				System.out.println(ex.getMessage());
				log.error(ex.getMessage());
			}

			log.info("DD_GPE30_ambiente_ISP :  " + System.getProperty("DD_GPE30_ambiente_ISP"));

			suffisso = "_LOC";
//			if (System.getProperty("DD_GPE30_ambiente_ISP")!=null)
//			{
//				if (System.getProperty("DD_GPE30_ambiente_ISP").contains("PRODUZIONE")) suffisso = "_PROD";
//				if (System.getProperty("DD_GPE30_ambiente_ISP").contains("SYSTEMTEST")) suffisso = "_TEST";		
//				if (System.getProperty("DD_GPE30_ambiente_ISP").contains("APPLICATION")) suffisso = "_SVIL";
//			}
//			
			log.info("SUFFISSO :  " + suffisso);

			dbdriver = GesConn.FindProperties("DBDRIVER" + suffisso);
			stringconnection = GesConn.FindProperties("STRINGCONNECTION" + suffisso);
			propsLoaded = true;

			log.info("logfile.path -->" + System.getProperty("logfile.path"));
			log.info("dbdriver -->" + dbdriver);
			log.info("stringconnection -->" + stringconnection);
			log.info("newConn -->" + GesConn.FindProperties("NEWCONN" + suffisso));
			log.info("jndiname -->" + GesConn.FindProperties("JNDINAME" + suffisso));
			log.info("*******************");
		}

	}

	public static Connection CreaConnessione(String msg) throws SQLException {
		return CreaConnessione(GesConn.FindProperties("LOGIN" + suffisso),
				GesConn.FindProperties("PASSWORD" + suffisso), msg);
	}

	private static Connection CreaConnessione(String login, String password, String msg) throws SQLException {
		Logger log = Logger.getLogger(GesConn.class);
		log.info("CREO connessione per ... " + msg);

		Connection connection = null;
		String newconn = GesConn.FindProperties("NEWCONN" + suffisso);
		try {
			if (newconn != null && newconn.equals("1")) {
				log.info("newConn ... " + newconn);
				String jndiname = GesConn.FindProperties("JNDINAME" + suffisso);
				InitialContext ic = new InitialContext();
				// DataSource ds = (DataSource) ic.lookup("java:/comp/env/"+jndiname);
				DataSource ds = (DataSource) ic.lookup(jndiname);
				connection = ds.getConnection();
			} else {
				log.info("newConn ... " + newconn);
				DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
				connection = DriverManager.getConnection(stringconnection, login, password);
			}
		}

		catch (SQLException ex) {
			log.error("SQLException : " + ex.getMessage());
			log.error("SQLState : " + ex.getSQLState());
			log.error("VendorError : " + ex.getErrorCode());
			throw ex;
		}

		catch (Exception e) {
			log.warn("Unable to load OracleDriver. Verificare il path!!");
			log.warn(e.getMessage());
		}
		return connection;
	}

	public static String FindProperties(String valore) {
		String v = (String) props.get(valore);
		return v;
	}

	/*
	 * public static void loadProperties() { props = new Properties(); try {
	 * props.load(new FileInputStream(configDir + "Resources.properties")); } catch
	 * (IOException e) { System.out.println(e.getMessage()); } }
	 */

}
