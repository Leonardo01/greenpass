package it.eng.services.bodies.dao.dto;

import java.util.Set;

public class InitFilterResponse {
	
	private String societa;
	private String filiale;
	private String indirizzoFiliale;
	private Set<String> distaccamento;
	private Set<String> indirizzoDistaccamento;
	
	public InitFilterResponse() {}
	
	public String getSocieta() {
		return societa;
	}
	public void setSocieta(String societa) {
		this.societa = societa;
	}
	public String getFiliale() {
		return filiale;
	}
	public void setFiliale(String filiale) {
		this.filiale = filiale;
	}
	public String getIndirizzoFiliale() {
		return indirizzoFiliale;
	}
	public void setIndirizzoFiliale(String indirizzoFiliale) {
		this.indirizzoFiliale = indirizzoFiliale;
	}
	public Set<String> getDistaccamento() {
		return distaccamento;
	}
	public void setDistaccamento(Set<String> distaccamento) {
		this.distaccamento = distaccamento;
	}
	public Set<String> getIndirizzoDistaccamento() {
		return indirizzoDistaccamento;
	}
	public void setIndirizzoDistaccamento(Set<String> indirizzoDistaccamento) {
		this.indirizzoDistaccamento = indirizzoDistaccamento;
	}
}
