package it.eng.services.bodies.dao.dto;

public class Greenpass {
	private Long id;
	private String dataVerifica;
	private String oraVerifica;
	private String nominativoIncaricato;
	private Integer nSoggInt;
	private Integer nSoggEst;
	private Boolean checked;

	
	public Greenpass() {}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getDataVerifica() {
		return dataVerifica;
	}


	public void setDataVerifica(String dataVerifica) {
		this.dataVerifica = dataVerifica;
	}


	public String getOraVerifica() {
		return oraVerifica;
	}


	public void setOraVerifica(String oraVerifica) {
		this.oraVerifica = oraVerifica;
	}


	public String getNominativoIncaricato() {
		return nominativoIncaricato;
	}


	public void setNominativoIncaricato(String nominativoIncaricato) {
		this.nominativoIncaricato = nominativoIncaricato;
	}


	public Integer getnSoggInt() {
		return nSoggInt;
	}


	public void setnSoggInt(Integer nSoggInt) {
		this.nSoggInt = nSoggInt;
	}


	public Integer getnSoggEst() {
		return nSoggEst;
	}


	public void setnSoggEst(Integer nSoggEst) {
		this.nSoggEst = nSoggEst;	
	}


	public Boolean getChecked() {
		return checked;
	}


	public void setChecked(Boolean checked) {
		this.checked = checked;
	}
	
	
}
